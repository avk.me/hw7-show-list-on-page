/*Ответ на теоритический вопрос:
DOM это интерфейс, позволяющий скриптам иметь доступ к HTML-элементам на странице.  
Реализован в виде дерева узлов.
*/

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin", "Brovary"], "Odessa", "Lviv", "Dnieper"];

//через forEach:
/*function showList(array, parent) {
    let ulElement = document.createElement("ul");
    ulElement.innerHTML = "The items of array are following:"
    parent.appendChild(ulElement);
    let list = array.forEach(function (item) {
        let itemToHTML = document.createElement("li");
        itemToHTML.innerHTML = item;
        return ulElement.appendChild(itemToHTML);
    })
}*/

//через map и шаблонные строки:
function showList(array, parent) {
    let ulElement = document.createElement("ul");
    parent.appendChild(ulElement);
    let list = array.map(function (item) {
        let liString;
        if (typeof (item) === 'object') {
            liString = `<ul><u><i>Suburbs:</i></u> ${item.map(function (subItem) {
                let subLiString;
                subLiString = `<li>${subItem}</li>`;
                return subLiString;
            })}</ul>`
        } else {
            liString = `<li>${item}</li>`;
        }
        return liString;
    });
    ulElement.innerHTML = "THE ITEMS OF ARRAY ARE FOLLOWING:" + `<p></p>` + list.toString().split(',').join('');
}

showList(array, document.body);

function timer() {
    let endDate = new Date(new Date().getTime() + 4000);
    let timer = document.createElement('h1')
    timer.innerHTML = "";
    document.body.appendChild(timer);

    setInterval(function () {
        let now = new Date();
        let seconds = (endDate.getSeconds() - now.getSeconds());
        timer.innerHTML = "Seconds left: " + seconds;
    }, 1000);
}

function deletePage() {
    document.body.innerHTML = "";
}

timer();
setTimeout(deletePage, 4000);